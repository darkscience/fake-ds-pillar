{% if grains['os'] == 'FreeBSD' %}
path: "/usr/local/"
admin_group: "wheel"
{% else %}
path: "/"
admin_group: "systems"
{% endif %}
