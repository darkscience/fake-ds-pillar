base:
  "E@^(celadon|cinnabar|cthulhu|indigo|lavender|saffron).darkscience.net$":
    - irc.admin.dijit
  '*-minion.e3.drk.sc':
    - irc.ssl
    - irc.passwords
    - irc.opers
    - irc.admin.dijit
  '*':
    - os
  '*.darkscience.net':
    - irc.ssl
    - irc.passwords
    - irc.opers
  "verus.darkscience.net":
    - irc.admin.derecho
  "pallet.darchoods.net":
    - irc.ssl
    - irc.passwords
    - irc.opers
    - irc.admin.darchoods
