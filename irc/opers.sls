#!yaml|gpg

opers:
  dijit:    <oper name="pwny_dijit" hash="sha256" password="" host="*" type="GlobalOper">
  derecho:  <oper name="pwny_derecho" hash="sha256" password="" host="*" type="GlobalOper">
  kylef:    <oper name="pwny_konky" hash="sha256" password="" host="*" type="GlobalOper">
  scub:     <oper name="pwny_scrubber" hash="sha256" password="" host="*" type="GlobalOper">
  xlink:    <oper name="pwny_supahlink" hash="sha256" password="" host="*" type="GlobalOper">
  narada:   <oper name="pwny_naradoodle" hash="sha256" password="" host="*" type="GlobalOper">
  liothen:  <oper name="pwny_lio" hash="sha256" password="" host="*" type="GlobalOper">

