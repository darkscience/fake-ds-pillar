#!yaml|gpg

passwords:
  mibbit: fakepassword_mibbit
  kiwi: fakepassword_kiwi
  cloak_key: 0xfa7e

links:
  celadon: SUPERPASSWORD4CELADON
  cthulhu: SUPERPASSWORD4CTHULHU
  zeta: SUPERPASSWORD4ZETA
